package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// createQuestion add a new Question
func createQuestion(c *gin.Context) {
	question := Question{
		Title: c.PostForm("title"),
	}
	db.Save(&question)
	c.JSON(http.StatusCreated, gin.H{"status": http.StatusCreated, "message": "Question item created successfully!", "resourceId": question.ID})
}

// fetchAllQuestion fetch all Questions
func fetchAllQuestion(c *gin.Context) {
	var questions []Question

	db.Find(&questions)
	if len(questions) <= 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Question found!"})
		return
	}

	for i := range questions {
		db.Model(questions[i]).Related(&questions[i].Answers)
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": questions})
}

// fetchSingleQuestion fetch a single Question
func fetchSingleQuestion(c *gin.Context) {
	var question Question
	questionID := c.Param("id")
	db.First(&question, questionID)
	db.Model(&question).Related(&question.Answers)
	if question.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Question found!"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": question})
}

// updateQuestion update a Question
func updateQuestion(c *gin.Context) {
	var question Question
	questionID := c.Param("id")
	db.First(&question, questionID)
	if question.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Question found!"})
		return
	}

	if c.PostForm("title") != "" {
		question.Title = c.PostForm("title")
	}

	db.Save(&question)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Question updated successfully!"})
}

// deleteQuestion remove a Question
func deleteQuestion(c *gin.Context) {
	var question Question
	questionID := c.Param("id")
	db.First(&question, questionID)
	if question.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Question found!"})
		return
	}
	db.Delete(&question)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Question deleted successfully!"})
}
