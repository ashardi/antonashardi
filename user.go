package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// createUser add a new User
func createUser(c *gin.Context) {
	user := User{
		Username: c.PostForm("username"),
		Password: hashAndSalt([]byte(c.PostForm("password"))),
		Email:    c.PostForm("email"),
		Name:     c.PostForm("name"),
		Role:     c.PostForm("role"),
	}
	db.Save(&user)
	c.JSON(http.StatusCreated, gin.H{"status": http.StatusCreated, "message": "User item created successfully!", "resourceId": user.ID})
}

// fetchAllUser fetch all Users
func fetchAllUser(c *gin.Context) {
	var users []User
	var _users []transformedUser

	db.Find(&users)

	if len(users) <= 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No User found!"})
		return
	}
	for _, item := range users {
		// fmt.Println("ID", item.ID)
		_users = append(_users, transformedUser{ID: item.ID, CreatedAt: item.CreatedAt, UpdatedAt: item.UpdatedAt, Username: item.Username, Email: item.Email, Name: item.Name, Role: item.Role})
	}
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": _users})
}

// fetchSingleUser fetch a single User
func fetchSingleUser(c *gin.Context) {
	var user User
	userID := c.Param("id")
	db.First(&user, userID)
	if user.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No User found!"})
		return
	}
	_user := transformedUser{ID: user.ID, CreatedAt: user.CreatedAt, UpdatedAt: user.UpdatedAt, Username: user.Username, Email: user.Email, Name: user.Name, Role: user.Role}
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": _user})
}

// updateUser update a user
func updateUser(c *gin.Context) {
	var user User
	userID := c.Param("id")
	db.First(&user, userID)
	if user.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No User found!"})
		return
	}

	if c.PostForm("email") != "" {
		user.Email = c.PostForm("email")
	}

	if c.PostForm("name") != "" {
		user.Name = c.PostForm("name")
	}

	if c.PostForm("role") != "" {
		user.Role = c.PostForm("role")
	}

	db.Save(&user)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "User updated successfully!"})
}

// deleteUser remove a User
func deleteUser(c *gin.Context) {
	var user User
	userID := c.Param("id")
	db.First(&user, userID)
	if user.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No User found!"})
		return
	}
	db.Delete(&user)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "User deleted successfully!"})
}
