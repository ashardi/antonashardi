package main

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// createTest add a new Test
func createTest(c *gin.Context) {
	correctPoint, _ := strconv.Atoi(c.PostForm("correct_point"))
	wrongPoint, _ := strconv.Atoi(c.PostForm("wrong_point"))
	test := Test{
		Title:        c.PostForm("title"),
		CorrectPoint: correctPoint,
		WrongPoint:   wrongPoint,
	}
	db.Save(&test)
	c.JSON(http.StatusCreated, gin.H{"status": http.StatusCreated, "message": "Test item created successfully!", "resourceId": test.ID})
}

// fetchAllTest fetch all Test
func fetchAllTest(c *gin.Context) {
	var tests []Test

	db.Find(&tests)

	if len(tests) <= 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Test found!"})
		return
	}

	for i := range tests {
		db.Model(tests[i]).Preload("Question").Related(&tests[i].TestDetails)
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": tests})
}

// fetchSingleTest fetch a single Test
func fetchSingleTest(c *gin.Context) {
	var test Test
	testID := c.Param("id")
	db.First(&test, testID)
	db.Model(test).Preload("Question").Related(&test.TestDetails)
	if test.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Test found!"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": test})
}

// updateTest update a Test
func updateTest(c *gin.Context) {
	var test Test
	testID := c.Param("id")
	db.First(&test, testID)
	if test.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Test found!"})
		return
	}

	if c.PostForm("title") != "" {
		test.Title = c.PostForm("title")
	}
	if c.PostForm("correct_point") != "" {
		correctPoint, _ := strconv.Atoi(c.PostForm("correct_point"))
		test.CorrectPoint = correctPoint
	}
	if c.PostForm("wrong_point") != "" {
		wrongPoint, _ := strconv.Atoi(c.PostForm("wrong_point"))
		test.WrongPoint = wrongPoint
	}

	db.Save(&test)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Test updated successfully!"})
}

// deleteTest remove a Test
func deleteTest(c *gin.Context) {
	var test Test
	testID := c.Param("id")
	db.First(&test, testID)
	if test.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Test found!"})
		return
	}
	db.Delete(&test)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Test deleted successfully!"})
}

// createTestDetail add a new TestDetail
func createTestDetail(c *gin.Context) {
	var test Test
	var question Question

	testID, _ := strconv.Atoi(c.PostForm("test_id"))
	tID := uint(testID)
	questionID, _ := strconv.Atoi(c.PostForm("question_id"))
	qID := uint(questionID)

	// Check if Test exist
	db.First(&test, tID)
	if test.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Test found!"})
		return
	}

	// Check if Question exist
	db.First(&question, qID)
	if question.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Question found!"})
		return
	}

	testdetail := TestDetail{
		TestID:     tID,
		QuestionID: qID,
	}
	db.Save(&testdetail)
	c.JSON(http.StatusCreated, gin.H{"status": http.StatusCreated, "message": "Test item created successfully!", "resourceId": testdetail.ID})
}

// fetchAllTestDetail fetch all Test Detail
func fetchAllTestDetail(c *gin.Context) {
	var testdetails []TestDetail

	db.Find(&testdetails)
	if len(testdetails) <= 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Test found!"})
		return
	}

	for i := range testdetails {
		db.Model(testdetails[i]).Related(&testdetails[i].Test)
		db.Model(testdetails[i]).Related(&testdetails[i].Question)
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": testdetails})
}

// fetchSingleTestDetail fetch a single TestDetail
func fetchSingleTestDetail(c *gin.Context) {
	var testdetail TestDetail
	testdetailID := c.Param("id")
	db.First(&testdetail, testdetailID)
	db.Model(testdetail).Related(&testdetail.Test)
	db.Model(testdetail).Related(&testdetail.Question)
	if testdetail.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Test found!"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": testdetail})
}

// updateTestDetail update a Test Detail
func updateTestDetail(c *gin.Context) {
	var testdetail TestDetail
	var test Test
	var question Question

	testID, _ := strconv.Atoi(c.PostForm("test_id"))
	tID := uint(testID)
	questionID, _ := strconv.Atoi(c.PostForm("question_id"))
	qID := uint(questionID)

	// Check if Test exist
	db.First(&test, tID)
	if test.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Test found!"})
		return
	}

	// Check if Question exist
	db.First(&question, qID)
	if question.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Question found!"})
		return
	}

	testdetailID := c.Param("id")
	db.First(&testdetail, testdetailID)
	if testdetail.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Test found!"})
		return
	}

	if c.PostForm("test_id") != "" {
		testdetail.TestID = tID
	}
	if c.PostForm("question_id") != "" {
		testdetail.QuestionID = qID
	}

	db.Save(&testdetail)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Test updated successfully!"})
}

// deleteTestDetail remove a Test Detail
func deleteTestDetail(c *gin.Context) {
	var testdetail TestDetail
	testdetailID := c.Param("id")
	db.First(&testdetail, testdetailID)
	if testdetail.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Test found!"})
		return
	}
	db.Delete(&testdetail)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Test deleted successfully!"})
}
