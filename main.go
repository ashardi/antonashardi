package main

import (
	"fmt"
	"reflect"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var db *gorm.DB

func init() {
	//open a db connection
	var err error
	db, err = gorm.Open("mysql", "root:root@/gotodo?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic("failed to connect database")
	}
	//Migrate all the schemas
	for _, model := range []interface{}{
		User{}, Question{}, Answer{},
		Test{}, TestDetail{}, Submission{},
		SubmissionDetail{}, Logs{}, todoModel{},
	} {
		if err := db.AutoMigrate(model).Error; err != nil {
			fmt.Println(err)
		} else {
			fmt.Println("Auto migrating", reflect.TypeOf(model).Name(), "...")
		}
	}

	// Migrate single schema
	// db.AutoMigrate(&todoModel{})
}

func main() {
	router := gin.Default()
	v1Todo := router.Group("/api/v1/todos")
	{
		v1Todo.POST("/", createTodo)
		v1Todo.GET("/", fetchAllTodo)
		v1Todo.GET("/:id", fetchSingleTodo)
		v1Todo.PATCH("/:id", updateTodo)
		v1Todo.DELETE("/:id", deleteTodo)
	}

	v1User := router.Group("/api/v1/users")
	{
		v1User.POST("/", createUser)
		v1User.GET("/", fetchAllUser)
		v1User.GET("/:id", fetchSingleUser)
		v1User.PATCH("/:id", updateUser)
		v1User.DELETE("/:id", deleteUser)
	}

	v1Question := router.Group("/api/v1/questions")
	{
		v1Question.POST("/", createQuestion)
		v1Question.GET("/", fetchAllQuestion)
		v1Question.GET("/:id", fetchSingleQuestion)
		v1Question.PATCH("/:id", updateQuestion)
		v1Question.DELETE("/:id", deleteQuestion)
	}

	v1Test := router.Group("/api/v1/tests")
	{
		v1Test.POST("/", createTest)
		v1Test.GET("/", fetchAllTest)
		v1Test.GET("/:id", fetchSingleTest)
		v1Test.PATCH("/:id", updateTest)
		v1Test.DELETE("/:id", deleteTest)
	}

	v1TestDetail := router.Group("/api/v1/testdetails")
	{
		v1TestDetail.POST("/", createTestDetail)
		v1TestDetail.GET("/", fetchAllTestDetail)
		v1TestDetail.GET("/:id", fetchSingleTestDetail)
		v1TestDetail.PATCH("/:id", updateTestDetail)
		v1TestDetail.DELETE("/:id", deleteTestDetail)
	}

	v1Answer := router.Group("/api/v1/answers")
	{
		v1Answer.POST("/", createAnswer)
		v1Answer.GET("/", fetchAllAnswer)
		v1Answer.GET("/:id", fetchSingleAnswer)
		v1Answer.PATCH("/:id", updateAnswer)
		v1Answer.DELETE("/:id", deleteAnswer)
	}

	v1Submission := router.Group("/api/v1/submissions")
	{
		v1Submission.POST("/", createSubmission)
		v1Submission.GET("/", fetchAllSubmission)
		v1Submission.GET("/:id", fetchSingleSubmission)
		v1Submission.PATCH("/:id", updateSubmission)
		v1Submission.DELETE("/:id", deleteSubmission)
	}

	v1SubmissionDetail := router.Group("/api/v1/submissiondetails")
	{
		v1SubmissionDetail.POST("/", createSubmissionDetail)
		v1SubmissionDetail.GET("/", fetchAllSubmissionDetail)
		v1SubmissionDetail.GET("/:id", fetchSingleSubmissionDetail)
		v1SubmissionDetail.PATCH("/:id", updateSubmissionDetail)
		v1SubmissionDetail.DELETE("/:id", deleteSubmissionDetail)
	}

	router.Run()
}
