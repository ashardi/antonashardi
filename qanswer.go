package main

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// createAnswer add a new Answer
func createAnswer(c *gin.Context) {
	questionID, _ := strconv.Atoi(c.PostForm("question_id"))
	qID := uint(questionID)
	status, _ := strconv.Atoi(c.PostForm("status"))

	answer := Answer{
		QuestionID: qID,
		Title:      c.PostForm("title"),
		Status:     status,
	}
	db.Save(&answer)
	c.JSON(http.StatusCreated, gin.H{"status": http.StatusCreated, "message": "Answer item created successfully!", "resourceId": answer.ID})
}

// fetchAllAnswer fetch all Answer
func fetchAllAnswer(c *gin.Context) {
	var answers []Answer

	db.Find(&answers)
	if len(answers) <= 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Answer found!"})
		return
	}

	for i := range answers {
		db.Model(answers[i]).Related(&answers[i].Question)
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": answers})
}

// fetchSingleAnswer fetch a single Answer
func fetchSingleAnswer(c *gin.Context) {
	var answer Answer

	answerID := c.Param("id")
	db.First(&answer, answerID)
	db.Model(answer).Related(&answer.Question)
	if answer.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Answer found!"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": answer})
}

// updateAnswer update a Answer
func updateAnswer(c *gin.Context) {
	var answer Answer
	answerID := c.Param("id")
	db.First(&answer, answerID)
	if answer.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Answer found!"})
		return
	}

	if c.PostForm("question_id") != "" {
		questionID, _ := strconv.Atoi(c.PostForm("question_id"))
		answer.QuestionID = uint(questionID)
	}
	if c.PostForm("title") != "" {
		answer.Title = c.PostForm("title")
	}
	if c.PostForm("status") != "" {
		status, _ := strconv.Atoi(c.PostForm("status"))
		answer.Status = status
	}

	db.Save(&answer)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Answer updated successfully!"})
}

// deleteAnswer remove a Answer
func deleteAnswer(c *gin.Context) {
	var answer Answer
	answerID := c.Param("id")
	db.First(&answer, answerID)
	if answer.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Answer found!"})
		return
	}
	db.Delete(&answer)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Answer deleted successfully!"})
}
