package main

import (
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// createSubmission add a new Submission
func createSubmission(c *gin.Context) {
	var test Test
	var user User

	testID, _ := strconv.Atoi(c.PostForm("test_id"))
	tID := uint(testID)
	userID, _ := strconv.Atoi(c.PostForm("user_id"))
	uID := uint(userID)

	// Check if Test exist
	db.First(&test, tID)
	if test.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Test found!"})
		return
	}

	// Check if User exist and User's role is Student
	db.Where("role = ?", "student").First(&user, uID)
	if user.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No User found, or, only Student allowed to take the try out!"})
		return
	}

	submission := Submission{
		TestID: tID,
		UserID: uID,
	}
	db.Save(&submission)
	c.JSON(http.StatusCreated, gin.H{"status": http.StatusCreated, "message": "Submission item created successfully!", "resourceId": submission.ID})
}

// fetchAllSubmission fetch all Submission
func fetchAllSubmission(c *gin.Context) {
	var submissions []Submission

	db.Find(&submissions)
	if len(submissions) <= 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Submission found!"})
		return
	}

	for i := range submissions {
		db.Model(submissions[i]).Related(&submissions[i].Test)
		db.Model(submissions[i]).Related(&submissions[i].User)
		db.Model(submissions[i]).Preload("Question").Preload("Answer").Related(&submissions[i].SubmissionDetails)
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": submissions})
}

// fetchSingleSubmission fetch a single Submission
func fetchSingleSubmission(c *gin.Context) {
	var submission Submission

	submissionID := c.Param("id")
	db.First(&submission, submissionID)
	db.Model(submission).Related(&submission.Test)
	db.Model(submission).Related(&submission.User)
	db.Model(submission).Preload("Question").Preload("Answer").Related(&submission.SubmissionDetails)
	if submission.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Submission found!"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": submission})
}

// updateSubmission update a Submission
func updateSubmission(c *gin.Context) {
	var submission Submission

	submissionID := c.Param("id")
	db.First(&submission, submissionID)
	db.Model(submission).Related(&submission.Test)
	db.Model(submission).Preload("Answer").Related(&submission.SubmissionDetails)
	db.Model(submission).Preload("Answer")
	if submission.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Submission found!"})
		return
	}

	// Set TZ
	loc, _ := time.LoadLocation("Asia/Jakarta")
	layout := "2006-01-02 15:04:05 -0700"

	if c.PostForm("test_id") != "" {
		testID, _ := strconv.Atoi(c.PostForm("test_id"))
		submission.TestID = uint(testID)
	}
	if c.PostForm("user_id") != "" {
		userID, _ := strconv.Atoi(c.PostForm("user_id"))
		submission.UserID = uint(userID)
	}

	if c.PostForm("ended_at") != "" {
		strEndedAt := c.PostForm("ended_at")
		parseEndedAt, _ := time.Parse(layout, strEndedAt)
		endedAt := parseEndedAt.In(loc)
		submission.EndedAt = endedAt

		// Count duration
		interval := submission.EndedAt.Sub(submission.CreatedAt)
		submission.Duration = interval.Seconds()

		// Get Test Data, Getting points for correct and wrong answer
		var cp, wp, totalScore int = submission.Test.CorrectPoint, submission.Test.WrongPoint, 0
		var totalAnswered, totalNotAnswered, totalCorrect, totalWrong int = 0, 0, 0, 0
		for i := range submission.SubmissionDetails {
			if (submission.SubmissionDetails[i].Answer.ID != 0) && (submission.SubmissionDetails[i].Answer.Status == 1) {
				totalScore += cp
				totalCorrect++
				totalAnswered++
			} else if (submission.SubmissionDetails[i].Answer.ID != 0) && (submission.SubmissionDetails[i].Answer.Status == 0) {
				totalScore += wp
				totalWrong++
				totalAnswered++
			} else {
				totalNotAnswered++
			}
		}
		submission.Score = totalScore
		submission.TotalAnswered = totalAnswered
		submission.TotalNotAnswered = totalNotAnswered
		submission.TotalCorrect = totalCorrect
		submission.TotalWrong = totalWrong
	}

	db.Save(&submission)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Submission updated successfully!"})
}

// deleteSubmission remove a Submission
func deleteSubmission(c *gin.Context) {
	var submission Submission
	submissionID := c.Param("id")
	db.First(&submission, submissionID)
	if submission.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Submission found!"})
		return
	}
	db.Delete(&submission)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Submission deleted successfully!"})
}

// createSubmissionDetail add a new Submission Detail
func createSubmissionDetail(c *gin.Context) {
	var submission Submission
	var question Question
	var testDetail []TestDetail

	// Set TZ
	loc, _ := time.LoadLocation("Asia/Jakarta")
	// layout := "2006-01-02 15:04:05 -0700"

	submissionID, _ := strconv.Atoi(c.PostForm("submission_id"))
	sID := uint(submissionID)
	questionID, _ := strconv.Atoi(c.PostForm("question_id"))
	qID := uint(questionID)
	answerID, _ := strconv.Atoi(c.PostForm("answer_id"))
	aID := uint(answerID)

	// Check if Submission exist
	db.First(&submission, sID)
	db.Model(submission).Related(&submission.Test)
	db.Model(submission).Related(&submission.SubmissionDetails)
	if submission.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Submission found!"})
		return
	}

	// Check if more than 15 minutes cannot submit answer anymore.
	timeNow := time.Now().In(loc)
	interval := timeNow.Sub(submission.CreatedAt)
	var duration = interval.Minutes()
	if duration > 15 {
		// Get answered items
		for j := range submission.SubmissionDetails {
			var ansID = submission.SubmissionDetails[j].QuestionID
			// Gather questions for this Submissions's test
			db.Where("test_id = ?", submission.TestID).Find(&testDetail)

			for i := range testDetail {
				// Then auto submit the rest of the questions with no answer
				if ansID != testDetail[i].QuestionID {
					submissiondetail := SubmissionDetail{
						SubmissionID: sID,
						QuestionID:   testDetail[i].QuestionID,
						AnswerID:     0,
					}
					db.Save(&submissiondetail)
				}
			}
		}

		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "Timeout! 15 minutes reached"})
		return
	}

	// Check if Question exist
	db.First(&question, qID)
	if question.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Question found!"})
		return
	}

	submissiondetail := SubmissionDetail{
		SubmissionID: sID,
		QuestionID:   qID,
		AnswerID:     aID,
	}
	db.Save(&submissiondetail)
	c.JSON(http.StatusCreated, gin.H{"status": http.StatusCreated, "message": "Submission item created successfully!", "resourceId": submissiondetail.ID})
}

// fetchAllSubmissionDetail fetch all Submission Detail
func fetchAllSubmissionDetail(c *gin.Context) {
	var submissiondetails []SubmissionDetail

	db.Find(&submissiondetails)
	if len(submissiondetails) <= 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Submission Detail found!"})
		return
	}

	for i := range submissiondetails {
		db.Model(submissiondetails[i]).Related(&submissiondetails[i].Submission)
		db.Model(submissiondetails[i]).Related(&submissiondetails[i].Question)
		db.Model(submissiondetails[i]).Related(&submissiondetails[i].Answer)
	}

	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": submissiondetails})
}

// fetchSingleSubmissiondDetail fetch a single Submission Detail
func fetchSingleSubmissionDetail(c *gin.Context) {
	var submissiondetail SubmissionDetail

	submissiondetailID := c.Param("id")
	db.First(&submissiondetail, submissiondetailID)
	db.Model(submissiondetail).Related(&submissiondetail.Submission)
	db.Model(submissiondetail).Related(&submissiondetail.Question)
	db.Model(submissiondetail).Related(&submissiondetail.Answer)
	if submissiondetail.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Submission found!"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": submissiondetail})
}

// updateSubmissionDetail update a Submission Detail
func updateSubmissionDetail(c *gin.Context) {
	var submission Submission
	var question Question
	var answer Answer

	var submissiondetail SubmissionDetail
	submissionID, _ := strconv.Atoi(c.PostForm("submission_id"))
	sID := uint(submissionID)
	questionID, _ := strconv.Atoi(c.PostForm("question_id"))
	qID := uint(questionID)
	answerID, _ := strconv.Atoi(c.PostForm("answer_id"))
	aID := uint(answerID)

	// Check if Submission exist
	db.First(&submission, sID)
	if submission.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Submission found!"})
		return
	}

	// Check if Question exist
	db.First(&question, qID)
	if question.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Question found!"})
		return
	}

	// Check if Answer exist
	db.First(&answer, aID)
	if answer.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Answer found!"})
		return
	}

	if c.PostForm("submission_id") != "" {
		submissiondetail.SubmissionID = sID
	}
	if c.PostForm("question_id") != "" {
		submissiondetail.QuestionID = qID
	}
	if c.PostForm("user_id") != "" {
		submissiondetail.AnswerID = aID
	}

	db.Save(&submissiondetail)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Submission Detail updated successfully!"})
}

// deleteSubmissionDetail remove a Submission Detail
func deleteSubmissionDetail(c *gin.Context) {
	var submissiondetail SubmissionDetail
	submissiondetailID := c.Param("id")
	db.First(&submissiondetail, submissiondetailID)
	if submissiondetail.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No Submission Detail found!"})
		return
	}
	db.Delete(&submissiondetail)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Submission Detail deleted successfully!"})
}
