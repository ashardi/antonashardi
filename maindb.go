package main

import (
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type (
	// User Model
	User struct {
		gorm.Model
		Username string `json:"username"`
		Password string `json:"password"`
		Email    string `json:"email"`
		Name     string `json:"name"`
		Role     string `json:"role"`
	}
	transformedUser struct {
		ID        uint      `json:"id"`
		Username  string    `json:"username"`
		Email     string    `json:"email"`
		Name      string    `json:"name"`
		Role      string    `json:"role"`
		CreatedAt time.Time `json:"CreatedAt"`
		UpdatedAt time.Time `json:"UpdatedAt"`
	}
	// Question Model
	Question struct {
		gorm.Model
		Title   string `json:"title"`
		Answers []Answer
	}
	// Answer Model
	Answer struct {
		gorm.Model
		Question   Question
		QuestionID uint   `json:"question_id"`
		Title      string `json:"title"`
		Status     int    `json:"status"`
	}
	// Test Model
	Test struct {
		gorm.Model
		Title        string `json:"title"`
		CorrectPoint int    `json:"correct_point"`
		WrongPoint   int    `json:"wrong_point"`
		TestDetails  []TestDetail
		Submission   []Submission
	}
	// TestDetail Model
	TestDetail struct {
		gorm.Model
		Test       Test
		TestID     uint `json:"test_id"`
		Question   Question
		QuestionID uint `json:"question_id"`
	}
	// Submission Model
	Submission struct {
		gorm.Model
		Test              Test
		TestID            uint `json:"test_id"`
		User              User
		UserID            uint      `json:"user_id"`
		Duration          float64   `json:"duration"`
		Score             int       `json:"score"`
		TotalAnswered     int       `json:"total_answered"`
		TotalNotAnswered  int       `json:"total_not_answered"`
		TotalCorrect      int       `json:"total_correct"`
		TotalWrong        int       `json:"total_wrong"`
		EndedAt           time.Time `json:"ended_at"`
		SubmissionDetails []SubmissionDetail
	}
	// SubmissionDetail Model
	SubmissionDetail struct {
		gorm.Model
		Submission   Submission
		SubmissionID uint `json:"submission_id"`
		Question     Question
		QuestionID   uint `json:"question_id"`
		Answer       Answer
		AnswerID     uint `json:"answer_id"`
	}
	// Logs Model
	Logs struct {
		gorm.Model
		Activity  string `json:"activity"`
		IPAddress int    `json:"ip_addresss"`
		User      User
		UserID    uint `json:"user_id"`
	}
	// todoModel describes a todoModel type
	todoModel struct {
		gorm.Model
		Title     string `json:"title"`
		Completed int    `json:"completed"`
	}
	// transformedTodo represents a formatted todo
	transformedTodo struct {
		ID        uint   `json:"id"`
		Title     string `json:"title"`
		Completed bool   `json:"completed"`
	}
)
